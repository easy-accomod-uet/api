const {validateString, validateBoolean, parseRangeValue} = require('../../helpers/parser')
const parseDateQuery = require('../../helpers/parseDateQuery')
const {escapeStringRegex} = require('../../helpers/parseRegex')

exports.getList = (query) => {
    const builtQuery = {}

    // for text search
    if (query.hasOwnProperty('nears')) builtQuery['$text'] = {$search: validateString(query.nears)}

    // same as manager
    if (query.hasOwnProperty('is_rejected')) builtQuery.rejected_at = {$exists: validateBoolean(query.is_rejected)}
    if (query.hasOwnProperty('is_approved')) builtQuery.approved_at = {$exists: validateBoolean(query.is_approved)}
    if (query.hasOwnProperty('is_rented')) builtQuery.rented_at = {$exists: validateBoolean(query.is_rented)}
    if (query.hasOwnProperty('is_expired')) {
        Object.assign(builtQuery, {
            approved_at: {$exists: true},
            expired_at: validateBoolean(query.is_expired) ? {$lte: new Date()} : {$gt: new Date()}
        })
    }
    if (query.hasOwnProperty('author')) builtQuery.author = validateString(query.author)
    if (query.hasOwnProperty('views')) builtQuery['views'] = parseRangeValue(query.views)
    if (query.hasOwnProperty('likes')) builtQuery['likes'] = parseRangeValue(query.likes)

    if (query.hasOwnProperty('price')) builtQuery['detail.rent_fee.amount'] = parseRangeValue(query.price)
    if (query.hasOwnProperty('electrict_fee')) builtQuery['detail.electrict_fee.amount'] = parseRangeValue(query.electrict_fee)
    if (query.hasOwnProperty('water_fee')) builtQuery['detail.water_fee.amount'] = parseRangeValue(query.water_fee)
    if (query.hasOwnProperty('room_type')) builtQuery['detail.type'] = validateString(query.room_type)
    if (query.hasOwnProperty('area')) builtQuery['detail.area'] = parseRangeValue(query.area)
    if (query.hasOwnProperty('shared')) builtQuery['detail.shared'] = validateBoolean(query.shared)
    if (query.hasOwnProperty('has_utilities')) builtQuery['detail.utilities.name'] = Array.isArray(query.has_utilities) ? {$in: query.has_utilities.map(e => e.trim().toLowerCase())} : query.has_utilities.trim().toLowerCase()

    if (query.hasOwnProperty('createdAt')) builtQuery.createdAt = parseDateQuery(query.createdAt)

    return builtQuery
}

const combineText = (texts) => {
    return texts.reduce((prev, text) => {
        return prev + (text || '')
    }, '')
}

exports.forAddress = (query) => {
    const builtQuery = {}

    if (query.hasOwnProperty('province')) builtQuery.province = {$regex: new RegExp(escapeStringRegex(validateString(query.province)), 'gi')}

    if (query.hasOwnProperty('district')) builtQuery.district = {$regex: new RegExp(escapeStringRegex(validateString(query.district)), 'gi')}

    if (query.hasOwnProperty('address')) builtQuery['$text'] = {$search: validateString(query.address)}

    return builtQuery
}

exports.manageList = (query) => {

    const builtQuery = {}

    if (query.hasOwnProperty('title')) builtQuery.title = {$regex: new RegExp(escapeStringRegex(validateString(query.title)), 'gi')}

    if (query.hasOwnProperty('is_rejected')) builtQuery.rejected_at = {$exists: validateBoolean(query.is_rejected)}
    if (query.hasOwnProperty('is_approved')) builtQuery.approved_at = {$exists: validateBoolean(query.is_approved)}
    if (query.hasOwnProperty('is_rented')) builtQuery.rented_at = {$exists: validateBoolean(query.is_rented)}
    if (query.hasOwnProperty('is_expired')) {
        Object.assign(builtQuery, {
            approved_at: {$exists: true},
            expired_at: validateBoolean(query.is_expired) ? {$lte: new Date()} : {$gt: new Date()}
        })
    }

    if (query.hasOwnProperty('author')) builtQuery.author = validateString(query.author)
    if (query.hasOwnProperty('views')) builtQuery['views'] = parseRangeValue(query.views)
    if (query.hasOwnProperty('likes')) builtQuery['likes'] = parseRangeValue(query.likes)

    if (query.hasOwnProperty('createdAt')) builtQuery.createdAt = parseDateQuery(query.createdAt)

    return builtQuery
}

exports.searchRate = (query) => {

    const builtQuery = {}

    if (query.hasOwnProperty('is_rejected')) builtQuery.rejected_at = {$exists: validateBoolean(query.is_rejected)}
    if (query.hasOwnProperty('is_approved')) builtQuery.approved_at = {$exists: validateBoolean(query.is_approved)}

    if (query.hasOwnProperty('user')) builtQuery.user = validateString(query.user)
    if (query.hasOwnProperty('post')) builtQuery.post = validateString(query.post)

    if (query.hasOwnProperty('createdAt')) builtQuery.createdAt = parseDateQuery(query.createdAt)

    return builtQuery
}
