const Fluentd = require('fluent-logger')

Fluentd.configure('easy-accommod', {
    host: '104.248.149.21',
    port: 32224,
    timeout: 3.0,
    reconnectInterval: 600000 // 10 minutes
})

module.exports = Fluentd
