const {response} = require('../common')
const PostAction = require('../actions/Post')
const AddressAction = require('../actions/Address')
const UserFavorite = require('../actions/UserFavorite')
const PostQueryBuilder = require('../actions/Post/QueryBuilder')
const OAuth = require('../middlewares/OAuth')
const PostParser = require('../actions/Post/PostParser')

exports.searchPost = response.wrapper(async (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.body)

    query.is_expired = false

    const queryAddress = PostQueryBuilder.forAddress(query)

    const builtQuery = PostQueryBuilder.getList(query)

    if (Object.keys(queryAddress).length > 0) {
        const addressFound = await AddressAction.find(queryAddress, '_id')

        builtQuery['detail.address'] = {$in: addressFound.map(a => a._id)}
    }

    const {data, meta} = await PostAction.getList(builtQuery, {page, limit}, {field, sort})

    data.list = data.list.map(post => PostParser.injectBoolField(post))

    return {
        data: data,
        meta: meta
    }
})

exports.getPost = response.wrapper(async (req, res) => {
    const {post_id: postId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)

    const post = await PostAction.getOne({_id: postId}, field)

    setImmediate(() => {
        PostAction.increaseLikeView(postId, {views: 1}).catch(console.log)
    })

    return {
        data: {
            post: PostParser.injectBoolField(post),
        },
        meta: {
            params: req.params,
            query: req.query,
        }
    }
})

exports.getRatePost = response.wrapper(async (req, res) => {
    const {post_id: postId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)

    const rates = await PostAction.getRatePost({post: postId}, field)

    return {
        data: {
            rates: rates,
        },
        meta: {
            params: req.params,
            query: req.query,
        }
    }
})


exports.likePost = response.wrapper(async (req, res) => {
    const {post_id: postId} = Object.assign({}, req.params)

    const post = await PostAction.increaseLikeView(postId, {likes: 1})

    return {
        data: true,
        meta: {
            params: req.params,
            query: req.query,
        }
    }
})

exports.reportPost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const vReason = await PostAction.reportPost(postId, _body, authPayload)

    return {
        data: true,
        meta: {
            body: _body,
        }
    }
})

exports.ratePost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const newRate = await PostAction.ratePost(postId, _body, authPayload)

    return {
        data: {
            rate: newRate,
        },
        meta: {
            body: _body,
        }
    }
})

exports.addToFavorite = response.wrapper(async (req, res) => {
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    const newUserFavourite = await UserFavorite.create({post_id: postId, user_id: userId})

    setImmediate(() => {
        PostAction.increaseLikeView(postId, {likes: 1}).catch(console.log)
    })

    return {
        data: newUserFavourite,
        // meta: {
        //     body: _body,
        // }
    }
})

