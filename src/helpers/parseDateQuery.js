const moment = require('moment')
const Checker = require('./Checker')

module.exports = (query) => {

    if (!Checker.isObject(query)) throw new Error(`invalid date query: ${query}`)

    const fromDate = query.from && moment().isValid(query.from) ? moment(query.from) : null

    if (!fromDate) throw new Error(`invalid from date: ${query.from}`)

    const toDate = query.to && moment(query.to).isValid() ? moment(query.to) : moment()

    const isDateValid = toDate.isAfter(fromDate)

    if (!isDateValid) throw new Error(`${query.from} is not start before ${query.to}`)

    return {
        '$gte': fromDate.toDate(),
        '$lte': toDate.toDate(),
    }
}
