const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Notification = getModel('Notification')

const _findById = (_id, select) => {
    const find = Notification.findById(_id)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (_id, field) => {
    const parsedField = parseField(field)

    const found = await _findById(_id, parsedField)

    if (!found) throw new Error(`Notification ${_id} not found`)

    return found
}
