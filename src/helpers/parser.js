const Checker = require('./Checker')

exports.validateString = (source) => {
    return typeof source === 'string' ? source.trim() : ''
}

exports.validateArray = (source) => {
    const arr = Array.isArray(source) ? source : []

    return arr.map(item => {
        if (typeof item === 'string') return item.trim()

        return item
    })
}

exports.validateNumber = (source) => {
    return !isNaN(+source) ? parseFloat(source) : null
}

exports.validateInteger = (source) => {
    return !isNaN(+source) ? parseInt(source) : 0
}

exports.validateBoolean = (source) => {
    return !!source
}

exports.parseRangeValue = (range) => {
    if (!Checker.isObject(range)) return range

    const parsed = {}

    if (range.hasOwnProperty('gt') || range.hasOwnProperty('$gt')) {
        parsed['$gt'] = range['gt'] || range['$gt'] || 0
    }

    if (range.hasOwnProperty('lt') || range.hasOwnProperty('$lt')) {
        parsed['$lt'] = range['lt'] || range['$lt'] || 0
    }

    if (range.hasOwnProperty('gte') || range.hasOwnProperty('$gte')) {
        parsed['$gte'] = range['gte'] || range['$gte'] || 0
    }

    if (range.hasOwnProperty('lte') || range.hasOwnProperty('$lte')) {
        parsed['$lte'] = range['lte'] || range['$lte'] || 0
    }

    return parsed
}
