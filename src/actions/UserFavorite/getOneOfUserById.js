const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const UserFavourite = getModel('UserFavourite')

const _findOne = (query, select) => {
    const find = UserFavourite.findOne(query).populate('user').populate({
        path: 'post',
        populate: {path: 'detail.address'}
    })

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (_id, userId, field) => {
    const parsedField = parseField(field)

    const found = await _findOne({_id, user: userId}, parsedField)

    if (!found) throw new Error(`UserFavourite ${_id} of User: ${userId} not found`)

    return found
}
