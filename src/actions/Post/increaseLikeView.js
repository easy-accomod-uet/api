const {getModel} = require('../../connections/database')

const Post = getModel('Post')

const updateById = (_id, data) => {
    return Post
        .findByIdAndUpdate(
            _id,
            data,
            {new: true}
        )
        .lean()
}

module.exports = async (_id, body) => {
    const dataToUpdate = {}
    if (body.hasOwnProperty('views')) {
        const vViews = parseInt(body.views, 10)
        if (Number.isNaN(vViews) || vViews < 0) throw new Error('invalid views value to increase')

        dataToUpdate["$inc"] = Object.assign({views: vViews}, dataToUpdate["$inc"])
    }

    if (body.hasOwnProperty('likes')) {
        const vLikes = parseInt(body.likes, 10)
        if (Number.isNaN(vLikes) || vLikes < 0) throw new Error('invalid likes value to increase')

        dataToUpdate["$inc"] = Object.assign({likes: vLikes}, dataToUpdate["$inc"])
    }

    const newPost = await updateById(_id, dataToUpdate)

    if (!newPost) throw new Error(`Post ${_id} not exists`)

    return newPost
}
