const {getModel} = require('../../connections/database')
const md5 = require('md5')
const _ = require('lodash')
const Joi = require('joi')

const User = getModel('User')

const findOneUser = async (query) => {
    return User.findOne(query).lean()
}

const isThatYou = (password, passwordCrypted) => {

    return md5(password) === passwordCrypted
}

const ChangePassSchema = Joi.object().keys({
    old_password: Joi.string().min(6).max(100).required(),
    new_password: Joi.string().min(6).max(100).required(),
})

module.exports = async (query, user) => {
    const vUser = await ChangePassSchema.validateAsync(user)

    const userFound = await findOneUser(query)

    if (!userFound) throw new Error(`User not found.`)

    if (!isThatYou(user.old_password, userFound.password)) throw new Error(`Wrong old password.`)

    if (vUser.old_password === vUser.new_password) throw new Error(`Old pass and new pass is the same.`)

    const newPassHash = md5(vUser.new_password)

    const newUser = await User.findByIdAndUpdate(userFound._id, {password: newPassHash}, {new: true})

    if (!newUser) throw new Error(`User not found.`)

    return _.omit(newUser, ['password'])
}
