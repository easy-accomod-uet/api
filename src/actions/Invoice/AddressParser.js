const _ = require('lodash')
const Joi = require('joi')

const AddressSchema = Joi.object().keys({
    province: Joi.string().trim().required(),
    district: Joi.string().trim().required(),
    address: Joi.string().trim().required(),
    updatedAt: Joi.date(),
    createdAt: Joi.date(),
})

const parseCreate = (data) => {
    if (!data) throw new Error('invalid data')

    return AddressSchema.validateAsync(data)
}

const parseDTO = (data) => {
    if (!data) throw new Error('invalid data')

    return _.omit(data, ['__v', 'createdAt', 'updatedAt'])
}

exports.parseCreate = parseCreate

exports.parseDTO = parseDTO

exports.AddressSchema = AddressSchema
