const _getTotalTime = (req) => {
    if (!req['_START_TIME']) return false

    const startTime = parseInt(req['_START_TIME'], 10)
    const finishTime = Date.now()

    const totalTime = finishTime - startTime

    if (!totalTime || totalTime <= 0) return false

    return totalTime
}

exports.sendSuccess = (req, res) => (result = {}) => {
    const totalTime = _getTotalTime(req)

    const vResult = Object.assign({}, result)

    if (process.env.NODE_ENV === 'production') {
        delete vResult.meta
    }

    const response = Object.assign({
        success: true,
    }, vResult)

    if (totalTime) {
        res.set('x-query-time', totalTime)
    }

    return res.status(vResult.status || 200).send(response)
}

exports.send404 = (req, res) => {
    const totalTime = _getTotalTime(req)

    const response = {
        success: false,
        message: '404 Not Found.'
    }

    if (totalTime) {
        res.set('x-query-time', totalTime)
    }

    return res.status(404).send(response)
}

exports.sendError = (req, res) => (error) => {
    const totalTime = _getTotalTime(req)

    const message = typeof error === 'string' ? error : error.message || ''
    const status = error.status || 400
    const reson = error.reson || false

    console.log("REQUEST_ERROR", error)

    const response = {
        success: false,
        message,
        errors: error,
    }

    if (reson) {
        response.reson = reson
    }

    if (totalTime) {
        res.set('x-query-time', totalTime)
    }

    res.status(status).send(response)
}

exports.catchError = (req, res) => error => {
    const totalTime = _getTotalTime(req)
    const message = typeof error === 'string' ? error : error.message || ''

    if (totalTime) {
        res.set('x-query-time', totalTime)
    }

    const response = {
        success: false,
        message
    }

    res.status(500).send(response)
}

exports.wrapper = (callback) => async (req, res) => callback(req, res).then(exports.sendSuccess(req, res)).catch(exports.sendError(req, res))
