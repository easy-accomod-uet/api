const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Post = getModel('Post')

const _findById = (query, select) => {
    const find = Post.findOne(query).populate('detail.address').populate('author').populate('rates')

    if (select) find.select(select)

    return find.lean({virtuals: true})
}

module.exports = async (_id, authPayload, field) => {
    const {_id: userId} = authPayload
    const parsedField = parseField(field)

    const found = await _findById({_id, author: userId}, parsedField)

    if (!found) throw new Error(`Post ${_id} of owner ${userId} not found`)

    return found

    // return {
    //     data: {
    //         post: found,
    //     },
    //     meta: {
    //         field: parsedField,
    //     }
    // }
}
