const {validateString, validateBoolean} = require('../../helpers/parser')
const parseDateQuery = require('../../helpers/parseDateQuery')
const {escapeStringRegex} = require('../../helpers/parseRegex')

exports.getList = (query) => {
    const builtQuery = {}

    // if (query.action) builtQuery.ation = {'$regex': new RegExp(escapeStringRegex(validateString(query.action)), 'gi')}
    // if (query.key) builtQuery.key = {'$regex': new RegExp(escapeStringRegex(validateString(query.key)), 'gi')}

    if (query.action) builtQuery.action = validateString(query.action)
    if (query.key) builtQuery.key = validateString(query.key)

    if (query.sender) builtQuery.sender = validateString(query.sender)
    if (query.receiver) builtQuery.receiver = validateString(query.receiver)

    if (query.is_new) builtQuery.is_new = validateBoolean(query.is_new)

    if (query.updatedAt) builtQuery.updatedAt = parseDateQuery(query.updatedAt)

    if (query.createdAt) builtQuery.createdAt = parseDateQuery(query.createdAt)

    return Object.assign(query, builtQuery)
}
