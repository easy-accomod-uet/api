FROM node:12-alpine

# set a directory for the app
WORKDIR /usr/src/app

# copy all the files to the container
COPY . .

# install dependencies
RUN npm install

# tell the port number the container should expose
EXPOSE 1900

ENV PORT=1900
ENV NODE_ENV=production

# run the command
CMD [ "node", "index.js" ]
