const {getModel} = require('../../connections/database')
const _ = require('lodash')
const parseField = require('../../helpers/parseField')
const User = getModel('User')

const _findById = (_id, select) => {
    const find = User.findById(_id)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (_id, field) => {
    const parsedField = parseField(field)

    const found = await _findById(_id, parsedField)

    if (!found) throw new Error(`User ${_id} not found`)

    return _.omit(found, ['password'])
}
