const parseDateQuery = require("../../helpers/parseDateQuery");
const { getModel } = require("../../connections/database");
const mongoose = require("mongoose");

const Submit = getModel("Submit");

exports.getTopScoreByQuiz = async (quiz_id, query) => {
  let conditions = {
    'user_quiz.quiz': new mongoose.Types.ObjectId(quiz_id),
  }
  if (query.from && query.to) {
    conditions.created_at = parseDateQuery(query)
  }
  let submits = await Submit.aggregate([
    {
      $lookup: {
        from: 'userquizzes',
        localField: 'user_quiz',
        foreignField: '_id',
        as: 'user_quiz'
      }
    },
    {
      $match: conditions,
    },
    {
      $sort: {
        'result.score': -1
      }
    },
    { $limit : 1 }
  ]);

  return {
    data: submits.length > 0 ? submits[0] : null,
  };
};