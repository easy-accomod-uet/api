const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
const QueryBuilder = require('./QueryBuilder')

const Notification = getModel('Notification')

const find = (query, {skip, limit}, {select, sort}) => {

    const find = Notification.find(query)
        .skip(skip)
        .limit(limit)
        .sort(sort || {is_new: -1, createdAt: -1})

    if (select) find.select(select)

    return find.lean()
}

const count = query => {
    return Notification.countDocuments(query)
}

module.exports = async (query, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const builtQuery = QueryBuilder.getList(query)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [data, total] = await Promise.all([
        find(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        count(builtQuery),
    ])

    return {
        data: {
            list: data,
            total,
            page: parsedPaging.page,
            limit: parsedPaging.limit,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            page: parsedPaging.page,
            limit: parsedPaging.limit,
        }
    }

    // return {
    //     data: {
    //         testCases: data,
    //     },
    //     meta: {
    //         query: builtQuery,
    //         field: parsedField,
    //         sort: parsedSort,
    //         total,
    //         page: parsedPaging.page,
    //         limit: parsePaging.limit,
    //     }
    // }
}
