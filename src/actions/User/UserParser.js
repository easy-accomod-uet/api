const Joi = require('joi')
const _ = require('lodash')
const regexUtils = require('../../helpers/regexUtils')

const UserSchema = Joi.object().keys({
    // username: Joi.string().alphanum().lowercase().min(5).max(30),
    // password: Joi.string().min(6).max(100),
    first_name: Joi.string().pattern(/^[a-zA-Z ]+$/),
    last_name: Joi.string().pattern(/^[a-zA-Z ]+$/),
    email: Joi.string().email(),
    birthday: Joi.date(),
    address: Joi.string().trim(),
    identification: Joi.string().trim(),
    phone: Joi.string().pattern(/^\+?\d+$/).min(8).max(20),
    role: Joi.string().lowercase().allow("renter", "owner", "admin"),
    // updatedAt: Joi.date(),
    // createdAt: Joi.date(),
})

const UserCreateSchema = Joi.object().keys({
    username: Joi.string().alphanum().lowercase().min(5).max(30).required(),
    password: Joi.string().min(6).max(100).required(),
    first_name: Joi.string().trim().pattern(regexUtils.vietnamese).required(),
    last_name: Joi.string().trim().pattern(regexUtils.vietnamese).required(),
    email: Joi.string().trim().email().required(),
    birthday: Joi.date().required(),
    address: Joi.string().trim(),
    identification: Joi.string().trim(),
    phone: Joi.string().pattern(/^\+?\d+$/).min(8).max(20).required(),
    role: Joi.string().lowercase().allow("renter", "owner", "admin"),
    // updatedAt: Joi.date(),
    // createdAt: Joi.date(),
})

const parseCreate = (data) => {
    if (!data) throw new Error('invalid data')

    return UserCreateSchema.validateAsync(data)
}

exports.parseCreate = parseCreate

exports.parseBase = (data) => {
    if (!data) throw new Error('invalid data')

    return UserSchema.validateAsync(data)
}

exports.omitFieldForUpdateProfile = (data) => {
    if (!data) throw new Error('invalid data')

    return _.omit(data, ['role', 'username', 'password', 'is_deleted', '_v', 'updatedAt', 'createdAt'])
}
