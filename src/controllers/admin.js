const {response} = require('../common')
const PostAction = require('../actions/Post')
const UserAction = require('../actions/User')
const NotificationAction = require('../actions/Notification')
const StatisticAction = require('../actions/Statistic')
const PostQueryBuilder = require('../actions/Post/QueryBuilder')
const PostParser = require('../actions/Post/PostParser')
const OAuth = require('../middlewares/OAuth')

const {actionEnum} = NotificationAction

exports.createUser = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)

    const newUser = await UserAction.create(_body)

    return {
        data: {
            user: newUser,
        },
        meta: {
            body: _body,
        }
    }
})

exports.searchUser = response.wrapper(async (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)

    const {data, meta} = await UserAction.getList(query, {page, limit}, {field, sort})

    return {
        data: data,
        meta: meta,
    }
})

exports.getUser = response.wrapper(async (req, res) => {
    const {user_id: userId} = Object.assign({}, req.params)

    const userFound = await UserAction.getById(userId)

    return {
        data: {
            user: userFound,
        },
    }
})

exports.updateUser = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {user_id: userId} = Object.assign({}, req.params)

    const userFound = await UserAction.updateById(userId, _body)

    return {
        data: {
            user: userFound,
        },
        meta: {
            body: _body,
        }
    }
})

exports.searchPost = response.wrapper(async (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    const builtQuery = PostQueryBuilder.manageList(query)

    const {data, meta} = await PostAction.getList(builtQuery, {page, limit}, {field, sort})

    data.list = data.list.map(post => PostParser.injectBoolField(post))

    return {
        data: data,
        meta: meta,
    }
})

exports.rankingPost = response.wrapper(async (req, res) => {
    const query = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)

    const builtQuery = PostQueryBuilder.manageList(query)
    builtQuery.top = query.top

    const data = await StatisticAction.rankingPost(builtQuery)

    return {
        data: data.map(post => PostParser.injectBoolField(post)),
    }
})

exports.getPost = response.wrapper(async (req, res) => {
    const {post_id: postId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)

    const post = await PostAction.getOne({_id: postId}, field)

    return {
        data: {
            post: PostParser.injectBoolField(post),
        },
        meta: {
            params: req.params,
            query: req.query,
        }
    }
})

exports.approvePost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId} = authPayload

    const newPost = await PostAction.approvePost(postId, _body, authPayload)

    setImmediate(() => {

        NotificationAction.create(actionEnum.ADMIN_APPROVE_POST, {sender: userId, receiver: newPost.author}, {key: 'post', description: `Your Post: ${newPost.title} has been approved and will expires in ${newPost.expired_at}`, meta: {post: newPost, expired_at: newPost.expired_at}})
    })

    return {
        data: true,
        meta: {
            body: _body,
        }
    }
})

exports.approveRatePost = response.wrapper(async (req, res) => {
    const {rate_id: rateId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId} = authPayload

    const newRate = await PostAction.approveRatePost(rateId)

    setImmediate(() => {

        NotificationAction.create(actionEnum.ADMIN_APPROVE_RATE_POST, {sender: userId, receiver: newRate.user}, {key: 'rate', description: `Your Rate: ${newRate.comment || newRate._id} has been approved by admin(${userId})`, meta: {approved_at: newRate.approved_at}})
    })

    return {
        data: {
            rate: newRate
        },
    }
})

exports.rejectRatePost = response.wrapper(async (req, res) => {
    const {rate_id: rateId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId} = authPayload

    const newRate = await PostAction.rejectRatePost(rateId)

    setImmediate(() => {

        NotificationAction.create(actionEnum.ADMIN_REJECT_RATE_POST, {sender: userId, receiver: newRate.user}, {key: 'rate', description: `Your Rate: ${newRate.comment || newRate._id} has been rejected by admin(${userId})`, meta: {rejected_at: newRate.rejected_at}})
    })

    return {
        data: {
            rate: newRate
        },
    }
})

exports.extendExpiresPost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId} = authPayload

    const newPost = await PostAction.extendExpires(postId, _body, authPayload)

    setImmediate(() => {
        NotificationAction.create(actionEnum.ADMIN_EXTEND_POST, {sender: userId, receiver: newPost.author}, {key: 'post', description: `Your Post: ${newPost.title} has been extend expires and will expires in ${newPost.expired_at}`, meta: {post: newPost, expired_at: newPost.expired_at}})
    })

    return {
        data: true,
        meta: {
            body: _body,
        }
    }
})


exports.rejectPost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId} = authPayload

    const {reason} = _body

    const vReason = reason || 'No reason.'

    const newPost = await PostAction.rejectPost(postId, _body, authPayload)

    setImmediate(() => {
        NotificationAction.create(actionEnum.ADMIN_REJECT_POST, {sender: userId, receiver: newPost.author}, {key: 'post', description: `Your Post: ${newPost.title} has been rejected by admin(${userId}). Reason: ${vReason}`, meta: {post: newPost, reason: vReason}})
    })

    return {
        data: true,
        meta: {
            body: _body,
        }
    }
})

exports.restorePost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId} = authPayload

    const {reason} = _body

    const vReason = reason || 'No reason.'

    const newPost = await PostAction.restorePost(postId, _body, authPayload)

    setImmediate(() => {
        NotificationAction.create(actionEnum.ADMIN_RESTORE_POST, {sender: userId, receiver: newPost.author}, {key: 'post', description: `Your Post: ${newPost.title} has been restore by admin(${userId}). Reason: ${vReason}`, meta: {post: newPost, reason: vReason}})
    })

    return {
        data: true,
        meta: {
            body: _body,
        }
    }
})

exports.createPost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    // const {_id: userId} = authPayload

    // const post = PostParser.omitForbiddenField(_body)
    const post = Object.assign({
        approved_at: Date.now(),
        expired_at: Date.now(),
    }, _body)

    const newPost = await PostAction.create(post, authPayload)

    // setImmediate(() => {
    //     NotificationAction.notiAllAdmin('create:post', {sender: userId}, {key: 'new-post', description: `Owner ${userId} create Post ${newPost._id}`, meta: {post: newPost}})
    // })

    return {
        data: {
            post: newPost,
        },
        meta: {
            body: post,
        }
    }
})

exports.updatePost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)
    // const {_id: userId} = authPayload

    if (_body.author) {
        authPayload._id = author
    }

    const post = Object.assign({
        approved_at: Date.now(),
        expired_at: Date.now(),
    }, _body)

    const newPost = await PostAction.updateById(postId, post, authPayload, true)

    // setImmediate(() => {
    //     NotificationAction.notiAllAdmin('update:post', {sender: userId}, {key: 'update-post', description: `Owner ${userId} update Post ${postId}`, meta: {post: newPost}})
    // })

    return {
        data: {
            post: newPost,
        },
        meta: {
            body: post
        }
    }
})

exports.updateAddress = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    if (_body.author) {
        authPayload._id = author
    }

    const newAddress = await PostAction.updateAddress(postId, _body, authPayload)

    return {
        data: {
            address: newAddress,
        },
        meta: {
            body: _body,
        }
    }
})

exports.listNotification = response.wrapper(async (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    // const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    query.receiver = userId

    const {data, meta} = await NotificationAction.getList(query, {page, limit}, {field, sort})

    return {
        data: data,
        meta: meta
    }
})

exports.viewNotification = response.wrapper(async (req, res) => {
    const {notification_id: notificationId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    const query = {
        _id: notificationId,
        receiver: userId,
    }

    const oldNoti = await NotificationAction.getOne(query)

    let noti = oldNoti

    if (noti.is_new) {
        const markAsViewed = {
            is_new: false,
        }

        noti = await NotificationAction.updateById(oldNoti._id, markAsViewed)
    }

    return {
        data: {
            notification: noti,
        },
        meta: {
            params: req.params,
        }
    }
})

exports.searchRates = response.wrapper(async (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    const builtQuery = PostQueryBuilder.searchRate(query)

    const {data, meta} = await PostAction.getListRates(builtQuery, {page, limit}, {field, sort})

    data.list = data.list.map(post => PostParser.injectRateField(post))

    return {
        data: data,
        meta: meta,
    }
})
