const jwt = require('jsonwebtoken')
const Checker = require('../helpers/Checker')

const JWT_SECRET_KEY = process.env.SECRET_KEY || 'uet123'

const signToken = async (payload, options = {}) => {
    if (!Checker.isObject(payload)) throw new Error('Invalid payload to sign.')

    const jwtOptions = Object.assign({
        expiresIn: '30d',
    }, options)

    return new Promise((resolve, reject) => {
        jwt.sign(payload, JWT_SECRET_KEY, jwtOptions, (error, token) => {
            if (error) {
                reject(error)

                return
            }

            resolve(token)
        })
    })
}

const verifyToken = async (token) => {

    return new Promise((resolve, reject) => {
        jwt.verify(token, JWT_SECRET_KEY, {}, (error, decoded) => {
            if (error) {
                reject(error)

                return
            }

            resolve(decoded)
        })
    })

}

const decodeToken = (token) => {

    return jwt.decode(token, {complete: true})
}

const getBearerToken = (req) => {
    const fromAuthorizationHeader = req.get('Authorization')
    const fromApiTokenHeader = req.get('api-token')

    const rawToken = (fromAuthorizationHeader || fromApiTokenHeader) || ''

    if (rawToken) {
        return rawToken.replace('Bearer ', '').trim()
    }

    return ''
}

const getPayloadToken = (req) => {
    return req['_payload_token'] || {}
}

const getHeaderToken = (req) => {
    return req['_header_token'] || {}
}

const injectPayloadToken = (req, res, next) => {
    const token = getBearerToken(req)

    if (!token) {
        res.status(403).send('No token provided.')

        return
    }

    const decodedToken = decodeToken(token)

    req['_payload_token'] = decodedToken.payload
    req['_header_token'] = decodedToken.header

    next()
}

const isAuthenticated = async (req, res, next) => {
    const token = getBearerToken(req)

    if (!token) {
        send403Forbidden(res, 'No token provided.')

        return
    }

    try {
        const payload = await verifyToken(token)

        // console.log(payload)

        req['_payload_token'] = payload
        req['_header_token'] = {}

        return next()
    } catch (e) {
        send403Forbidden(res, e.message)

        return
    }
}

const getUserId = (req) => {
    const payload = getPayloadToken(req)

    return payload.user_id || payload._id
}

const getUserRole = (req) => {
    const payload = getPayloadToken(req)

    return payload.role
}

const send403Forbidden = (res, message = 'Forbidden') => {
    res.status(403).send(`403 Forbidden. easyaccomod-uet@2020\nReason: ${message}`)
}

const parseRole = (listRoles) => {
    if (!listRoles) return []

    const roles = listRoles.split('|').filter(Boolean)

    return roles
}

const hasRole = (role) => (req, res, next) => {
    const roles = [getUserRole(req)]

    if (!roles) {
        send403Forbidden(res)

        return
    }

    // const roles = parseRole(listRoles)

    if (!roles.includes(role)) {
        send403Forbidden(res)

        return
    }

    next()
}

exports.getPayloadToken = getPayloadToken

exports.getHeaderToken = getHeaderToken

exports.injectPayloadToken = injectPayloadToken

exports.hasRole = hasRole

exports.generateToken = signToken
exports.isAuthenticated = isAuthenticated

exports.getUserRole = getUserRole
exports.getUserId = getUserId

exports.getBearerToken = getBearerToken
