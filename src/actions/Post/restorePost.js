const {getModel} = require('../../connections/database')
const NotificationAction = require('../Notification')

const Post = getModel('Post')

const _findPost = (query, select) => {
    const find = Post.findOne(query)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (postId, body, authPayload) => {
    const {_id: userId} = authPayload

    const postFound = await _findPost({_id: postId})

    if (!postFound) throw new Error(`Post ${postId} not found`)

    const {approved_at, rejected_at} = postFound

    if (approved_at) throw new Error(`This post has been approved.`)

    if (!rejected_at) throw new Error(`This post hasn't rejected yet.`)

    const {reason} = body

    const vReason = reason || 'No reason.'

    const updatedData = {
        $set: {
            check_by: userId,
        },
        $unset: {
            approved_at: 1,
            rejected_at: 1,
        },
    }

    const newPost = await Post.findByIdAndUpdate(postId, updatedData, {new: true})

    return newPost

    // setImmediate(() => {
    //     NotificationAction.create('rejected:post', {sender: userId, receiver: author}, {key: 'admin-reject-post', description: `Your Post: ${newPost.title} has been rejected by admin(${userId}). Reason: ${vReason}`, meta: {post: newPost, reason: vReason}})
    // })

    // return {
    //     data: true,
    //     meta: {
    //         body,
    //     }
    // }
}
