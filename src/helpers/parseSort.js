const Checker = require('./Checker')

const parseSort = (sort) => {
    if (!Checker.isString(sort) || !sort) return null

    const vSort = sort.toLowerCase()

    if (vSort[0] === '-') return {
        [vSort.substr(1)]: -1,
    }

    if (vSort[0] === '+') return {
        [vSort.substr(1)]: -1,
    }

    return {
        [vSort]: 1,
    }
}

module.exports = (sorts) => {
    if (!Checker.isString(sorts) || !sorts) return null

    const vSorts = sorts
        .split(/[,\s;.]/g)
        .map(parseSort)
        .filter(Boolean)

    if (!vSorts.length) return null

    return vSorts
        .reduce((acc, sort) => {
            return Object.assign(acc, sort)
        }, {})
}
