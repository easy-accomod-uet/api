const {getModel} = require('../../connections/database')

const Notification = getModel('Notification')

module.exports = async (action, info, data) => {
    const {receivers, sender} = info
    const {key, description, meta} = data

    const bulkWriteRecords = receivers.map(receiver => {
        return {
            insertOne: {
                document: {
                    receiver,
                    sender,
                    action,
                    key,
                    description,
                    meta: meta || {},
                }
            }
        }
    })

    const results = await Notification.bulkWrite(bulkWriteRecords)

    return results
}
