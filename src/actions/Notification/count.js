const {getModel} = require('../../connections/database')
const QueryBuilder = require('./QueryBuilder')

const Notification = getModel('Notification')

const count = query => {
    return Notification.countDocuments(query)
}

module.exports = async (query) => {
    const builtQuery = QueryBuilder.getList(query)

    const total = count(builtQuery)

    return total
}
