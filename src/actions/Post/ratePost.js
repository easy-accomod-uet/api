const Joi = require('joi')
const {getModel} = require('../../connections/database')
const NotificationAction = require('../Notification')

const {actionEnum} = NotificationAction

const Post = getModel('Post')
const Rate = getModel('Rate')

const _findPost = (query, select) => {
    const find = Post.findOne(query)

    if (select) find.select(select)

    return find.lean()
}

const rateSchema = Joi.object().keys({
    comment: Joi.string().trim(),
    rate: Joi.number().min(0).max(5).required(),
})

const _findRate = (query) => {

    return Rate.findOne(query).lean()
}

module.exports = async (postId, body, authPayload) => {
    const {_id: userId} = authPayload

    const vRate = await rateSchema.validateAsync(body)

    const findPost = _findPost({_id: postId})
    const findRate = _findRate({user: userId, post: postId})

    const [postFound, rateFound] = await Promise.all([findPost, findRate])

    if (!postFound) throw new Error(`Post ${postId} not found`)

    const {approved_at, rejected_at} = postFound

    if (!approved_at) throw new Error(`This post hasn't approved yet.`)

    if (rejected_at) throw new Error(`This post has been rejected.`)

    if (rateFound) {
        const {approved_at: approvedRate} = rateFound

        if (!approvedRate) throw new Error(`You have pending rate hasn't approved yet for this post.`)

        await Rate.findByIdAndUpdate(rateFound._id, {is_deleted: true})
    }

    const newRate = new Rate(Object.assign(vRate, {
        user: userId,
        post: postId,
    }))

    newRate.save()

    await NotificationAction.notiAllAdmin(actionEnum.RENTER_RATE_POST, {sender: userId}, {key: 'rate', description: `Renter: ${userId} Rate Post: ${postFound._id}. rate: ${vRate.rate}.`, meta: {post: postFound, rate: newRate.toJSON()}})

    return newRate.toJSON()
}
