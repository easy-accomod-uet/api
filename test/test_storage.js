const Uploader = require('@codeatest/uploader')
const path = require('path')

const DEFAULT_CREDENTIALS = path.join(__dirname, '../code-and-test-1a7e7a4f4c05.json')
const DEFAULT_PROJECT_ID = 'code-and-t'
const DEFAULT_BUCKET_NAME = 'codeatest'

setImmediate(async () => {
    const uploader = new Uploader({
        credentials: DEFAULT_CREDENTIALS,
        projectId: DEFAULT_PROJECT_ID,
        bucketName: DEFAULT_BUCKET_NAME,
    })

    try {

        // upload
        const result = await uploader.uploadFile(path.join(__dirname, '../index.js'), {destination: 'code/js/test.js'})

        // console.log(result)

        const signedUrl = await uploader.generateSignedUrl('code/js/test.js')

        console.log(signedUrl)

        process.exit(0)
    } catch (e) {

        console.log(e)

        process.exit(1)
    }
})
