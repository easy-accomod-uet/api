const parseDateQuery = require("../../helpers/parseDateQuery");
const { getModel } = require("../../connections/database");
const mongoose = require("mongoose");

const Submit = getModel("Submit");

exports.getTopScoreByTopic = async (topic_id, query) => {
  let conditions = {
    'user_topic.topic': new mongoose.Types.ObjectId(topic_id),
  }
  if (query.from && query.to) {
    conditions.created_at = parseDateQuery(query)
  }
  let submits = await Submit.aggregate([
    {
      $lookup: {
        from: 'usertopics',
        localField: 'user_topic',
        foreignField: '_id',
        as: 'user_topic'
      }
    },
    {
      $match: conditions,
    },
    {
      $sort: {
        'result.score': -1
      }
    },
    { $limit : 1 }
  ]);

  return {
    data: submits.length > 0 ? submits[0] : null,
  };
};