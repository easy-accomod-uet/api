const {getModel} = require('../../connections/database')
const bulkCreate = require('./bulkCreate')

const User = getModel('User')

module.exports = async (action, info, data) => {
    const allAdmin = await User.find({role: 'admin'}).select('_id').lean()

    info.receivers = allAdmin.map(u => u._id)

    return bulkCreate(action, info, data)
}
