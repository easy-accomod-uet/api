const {getModel} = require('../../connections/database')
const AddressParser = require('../Address/AddressParser')

const Address = getModel('Address')
const Post = getModel('Post')

const _findPost = (postId, userId) => {
    return Post.findOne({_id: postId, author: userId}).select('_id detail').lean()
}

module.exports = async (postId, address, authPayload) => {
    const {_id: userId} = authPayload

    const postFound = await _findPost(postId, userId)

    if (!postFound) throw new Error(`post:${postId} of owner:${userId} does not exists`)

    const addressId = postFound.detail.address

    const addressFound = await Address.findById(addressId).lean()

    if (!addressFound) throw new Error(`cannot found address of this post`)

    const mergedAddress = AddressParser.parseDTO(Object.assign({}, addressFound, address))

    delete mergedAddress._id

    const vAddress = await AddressParser.parseCreate(mergedAddress)

    const newAddress = await Address.findByIdAndUpdate(addressId, vAddress, {new: true}).lean()

    return newAddress

    // return {
    //     data: {
    //         address: newAddress,
    //     },
    //     meta: {
    //         body: address,
    //     }
    // }
}
