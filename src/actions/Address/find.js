const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Address = getModel('Address')

const _find = (query, select) => {
    const find = Address.find(query)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (query, field) => {
    const parsedField = parseField(field)

    const found = await _find(query, parsedField)

    return found
}
