const {getModel} = require('../../connections/database')

const Notification = getModel('Notification')

module.exports = async (notis) => {
    if (!Array.isArray(notis) || !notis.length) return true

    const notiIds = notis.map(noti => {
        const {_id, is_new} = noti

        if (!is_new) return false

        return _id
    }).filter(Boolean)

    if (!notiIds.length) return true

    const markAsViewed = {
        is_new: false,
    }

    const results = await Notification.updateMany({_id: {$in: notiIds}}, markAsViewed)

    return results
}
