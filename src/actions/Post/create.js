const {getModel} = require('../../connections/database')
const PostParser = require('./PostParser')
const AddressParser = require('../Address/AddressParser')
const NotificationAction = require('../Notification')

const Post = getModel('Post')
const Address = getModel('Address')


const createOneAddress = async (address) => {
    const vAddress = await AddressParser.parseCreate(address)

    const newAddress = new Address(vAddress)

    const doc = await newAddress.save()

    return doc.toJSON()
}

const createOnepost = async (post) => {
    const newpost = new Post(post)

    const doc = await newpost.save()

    return doc.toJSON()
}

module.exports = async (post, authPayload) => {
    const {_id: userId} = authPayload
    if (!userId) throw new Error(`missing user_id`)

    post.author = userId.toString()

    const parsedPost = await PostParser.parseCreate(post)

    const newAddress = await createOneAddress(parsedPost.detail.address)

    parsedPost.detail.address = newAddress._id

    const newPost = await createOnepost(parsedPost)

    return newPost

    // setImmediate(() => {
    //     NotificationAction.notiAllAdmin('create:post', {sender: userId}, {key: 'new-post', description: `Owner ${userId} create Post ${newPost._id}`, meta: {post: newPost}})
    // })
    //

    // return {
    //     data: {
    //         post: newPost,
    //     },
    //     meta: {
    //         body: post,
    //     }
    // }
}
