const {getModel} = require('../../connections/database')
const bulkCreate = require('./bulkCreate')

const User = getModel('User')

module.exports = async (action, info, data) => {
    const allOwner = await User.find({role: 'owner'}).select('_id').lean()

    info.receivers = allOwner.map(u => u._id)

    return bulkCreate(action, info, data)
}
