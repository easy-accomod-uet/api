const momentRange = require('moment-range')
const moment = momentRange.extendMoment(require('moment'))

const range = moment.range('2020-01-01 00:00', '2020-02-01 00:00')

const now = Date.now()

console.log(moment(now).startOf('hour').toISOString())

const hours = Array.from(range.by('hour'))

console.log((new Date()) - (new Date(now)))

// console.log(hours)
