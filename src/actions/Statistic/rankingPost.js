const Moment = require('../../helpers/Moment')
const PostParser = require('../Post/PostParser')
const {getModel} = require("../../connections/database")

const Post = getModel('Post')

module.exports = async (query = {}) => {
    const {top, ...queryFind} = query

    const topPost = await Post.find(queryFind).populate('author').populate('detail.address').populate('rates').sort({likes: -1, views: -1}).limit(top || 10).lean()

    return topPost
}
