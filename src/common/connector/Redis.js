const redis = require("redis")

const bluebird = require('bluebird')
bluebird.promisifyAll(redis.RedisClient.prototype)
bluebird.promisifyAll(redis.Multi.prototype)

module.exports = (args) => {
    const argsValidated = Object.assign({}, args)
    const client = redis.createClient(argsValidated)

    client.on('connect', () => {
        console.log('REDIS:connected')
    })

    client.on('ready', () => {
        console.log('REDIS:ready')
    })

    client.on('reconnecting', () => {
        console.log('REDIS:reconnecting')
    })

    client.on('end', () => {
        console.log('REDIS:disconnected')
    })

    client.on('error', (error) => {
        console.error('REDIS_ERROR', error)

        return process.exit(1)
    })

    return client
}
