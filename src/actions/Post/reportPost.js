const {getModel} = require('../../connections/database')
const NotificationAction = require('../Notification')

const {actionEnum} = NotificationAction

const Post = getModel('Post')

const _findPost = (query, select) => {
    const find = Post.findOne(query)

    if (select) find.select(select)

    return find.lean()
}

const LIMIT_REQUEST = 5

module.exports = async (postId, body, authPayload) => {
    const {_id: userId} = authPayload

    const postFound = await _findPost({_id: postId})

    if (!postFound) throw new Error(`Post ${postId} not found`)

    const {approved_at, rejected_at} = postFound

    if (!approved_at) throw new Error(`This post hasn't approved yet.`)

    if (rejected_at) throw new Error(`This post has been rejected.`)

    const {reason} = body

    const vReason = reason || 'No reason.'

    const numberOfReportPending = await NotificationAction.count({sender: userId, action: actionEnum.RENTER_REPORT_POST, is_new: true})

    if (numberOfReportPending >= LIMIT_REQUEST) throw new Error(`You've reached the maximum pending report(${LIMIT_REQUEST}), try again later!`)

    await NotificationAction.notiAllAdmin(actionEnum.RENTER_REPORT_POST, {sender: userId}, {key: 'post', description: `Renter: ${userId} report Post: ${postFound._id}. Reason: ${vReason}.`, meta: {post: postFound, reason: vReason}})

    return vReason
}
