const {getModel} = require('../../connections/database')
const UserFavourite = getModel('UserFavourite')

module.exports = async (_id, userId) => {

    const found = await UserFavourite.findOne({_id, user: userId}).lean()

    if (!found) throw new Error(`UserFavourite ${_id} of User: ${userId} not found`)

    await UserFavourite.findOneAndDelete({_id, user: userId})

    return true
}
