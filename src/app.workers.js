const schedule = require('node-schedule')
const ScanPostActivity = require('./workers/ScanPostActivity')

module.exports = () => {
    console.log(`REGISTER WORKERS`)

    schedule.scheduleJob('* * * * *', ScanPostActivity)
}
