const {response} = require('../common')
const PostAction = require('../actions/Post')
const StatisticAction = require('../actions/Statistic')
const NotificationAction = require('../actions/Notification')
const InvoiceAction = require('../actions/Invoice')
const PostParser = require('../actions/Post/PostParser')
const PostQueryBuilder = require('../actions/Post/QueryBuilder')
const OAuth = require('../middlewares/OAuth')

const {actionEnum} = NotificationAction

exports.searchPost = response.wrapper(async (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload
    query.author = userId

    const builtQuery = PostQueryBuilder.manageList(query)

    const {data, meta} = await PostAction.getList(builtQuery, {page, limit}, {field, sort})

    data.list = data.list.map(post => PostParser.injectBoolField(post))

    return {
        data: data,
        meta: meta
    }
})

exports.getPost = response.wrapper(async (req, res) => {
    const {post_id: postId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)
    const authPayload = OAuth.getPayloadToken(req)

    const post = await PostAction.getOwnerPost(postId, authPayload, field)

    return {
        data: {
            post: PostParser.injectBoolField(post),
        },
        meta: {
            params: req.params,
            query: req.query,
        }
    }
})

exports.statisticOnePost = response.wrapper(async (req, res) => {
    const query = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const data = await StatisticAction.detailOnePost(postId, query)

    return {
        data,
    }
})

exports.rankingPost = response.wrapper(async (req, res) => {
    const query = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload
    query.author = userId

    const builtQuery = PostQueryBuilder.manageList(query)
    builtQuery.top = query.top

    const data = await StatisticAction.rankingPost(builtQuery)

    return {
        data: data.map(post => PostParser.injectBoolField(post)),
    }
})

exports.markPostRented = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId} = authPayload

    const newPost = await PostAction.markAsRented(postId, _body, authPayload)

    setImmediate(() => {
        NotificationAction.notiAllAdmin(actionEnum.OWNER_MARK_RENTED_POST, {sender: userId}, {key: 'post', description: `Owner: ${userId} mark as rented Post: ${newPost._id}`, meta: {post: newPost, rented_at: newPost.rented_at}})
    })

    return {
        data: true,
        meta: {
            body: _body,
        }
    }
})

exports.previewInvoice = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)

    const value = await InvoiceAction.preview(_body, authPayload)

    return {
        data: {
            value: value,
        },
        meta: {
            body: _body,
        }
    }
})

exports.requestExtendExpiresPost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    // const {_id: userId} = authPayload

    const vExpiredAt = await PostAction.requestExtendExpires(postId, _body, authPayload)

    return {
        data: true,
        meta: {
            body: _body,
        }
    }
})

exports.createPost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    const post = PostParser.omitForbiddenField(_body)

    const newPost = await PostAction.create(post, authPayload)

    setImmediate(() => {
        NotificationAction.notiAllAdmin(actionEnum.OWNER_CREATE_POST, {sender: userId}, {key: 'post', description: `Owner: ${userId} create Post: ${newPost._id}`, meta: {post: newPost}})
    })

    return {
        data: {
            post: newPost,
        },
        meta: {
            body: post,
        }
    }
})

exports.updatePost = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)
    // const {_id: userId} = authPayload

    const post = PostParser.omitForbiddenField(_body)

    const newPost = await PostAction.updateById(postId, post, authPayload)

    // setImmediate(() => {
    //     NotificationAction.notiAllAdmin('update:post', {sender: userId}, {key: 'update-post', description: `Owner ${userId} update Post ${postId}`, meta: {post: newPost}})
    // })

    return {
        data: {
            post: newPost,
        },
        meta: {
            body: post
        }
    }
})

exports.updateAddress = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const {post_id: postId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    const newAddress = await PostAction.updateAddress(postId, _body, authPayload)

    return {
        data: {
            address: newAddress,
        },
        meta: {
            body: address,
        }
    }
})

exports.listNotification = response.wrapper(async (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    // const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    query.receiver = userId

    const {data, meta} = await NotificationAction.getList(query, {page, limit}, {field, sort})

    return {
        data: data,
        meta: meta
    }
})

exports.viewNotification = response.wrapper(async (req, res) => {
    const {notification_id: notificationId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    const query = {
        _id: notificationId,
        receiver: userId,
    }

    const oldNoti = await NotificationAction.getOne(query)

    let noti = oldNoti

    if (noti.is_new) {
        const markAsViewed = {
            is_new: false,
        }

        noti = await NotificationAction.updateById(oldNoti._id, markAsViewed)
    }

    return {
        data: {
            notification: noti,
        },
        meta: {
            params: req.params,
        }
    }
})
