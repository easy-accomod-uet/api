const multer = require('multer')
const Uploader = require('../services/Uploader')
const Path = require('path')

const upload = multer({dest: Path.join(__dirname, '../../.temp-upload')})

const uploadCode = fieldName => (req, res, next) => {

    const uploadToGG = async () => {
        const file = req.file
        const {language} = Object.assign({}, req.body)

        await Uploader.uploadFile(file.path, {
            destination: `code/${language}/${file.filename}`
        })

        next()
    }

    return upload.single(fieldName)(req, res, uploadToGG)
}

upload.uploadCode = uploadCode

module.exports = upload
