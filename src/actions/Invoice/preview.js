const {getModel} = require('../../connections/database')
const moment = require('moment')
const Joi = require('joi')

const ExpiredAtSchema = Joi.date().required()

const checkExpiredAt = (expiredAt) => ExpiredAtSchema.validateAsync(expiredAt)

const RATE = 3000

module.exports = async (body, authPayload) => {
    const {expired_at: expiredAt} = body

    const vExpiredAt = await checkExpiredAt(expiredAt)

    const duration = (new Date(vExpiredAt)) - Date.now()

    const durationHours = duration / 1000 / 60 / 60

    const value = durationHours > 0 ? durationHours * RATE : 0

    const pValue = Math.ceil(value)

    return pValue
}
