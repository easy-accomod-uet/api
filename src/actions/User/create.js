const {getModel} = require('../../connections/database')
const md5 = require('md5')
const _ = require('lodash')
const UserParser = require('./UserParser')

const User = getModel('User')

const createOneUser = async (user) => {
    const newUser = new User(user)

    const doc = await newUser.save()

    return doc.toJSON()
}

const prepareNewUser = (user) => {

    const passwordCrypted = md5(user.password)

    return Object.assign({}, user, {password: passwordCrypted})
}


module.exports = async (user) => {
    const validUser = await UserParser.parseCreate(user)

    const parsedUser = prepareNewUser(validUser)

    const newUser = await createOneUser(parsedUser)

    return {
        data: {
            user: _.omit(newUser, ['password']),
        },
        meta: {
            body: user,
        },
        status: 201,
    }
}
