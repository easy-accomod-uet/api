module.exports = {
    // Renter
    RENTER_REPORT_POST: 'renter:report:post',
    RENTER_RATE_POST: 'renter:rate:post',

    // Owner
    OWNER_CREATE_POST: 'owner:create:post',
    OWNER_UPDATE_POST: 'owner:update:post',
    OWNER_REQUEST_EXTEND_POST: 'owner:request-extend:post-expires',
    OWNER_MARK_RENTED_POST: 'owner:mark-rented:post',

    // Admin
    ADMIN_APPROVE_POST: 'admin:approve:post',
    ADMIN_REJECT_POST: 'admin:reject:post',
    ADMIN_RESTORE_POST: 'admin:restore:post',
    ADMIN_EXTEND_POST: 'admin:extend:post,expires',
    ADMIN_APPROVE_RATE_POST: 'admin:approve:post-rate',
    ADMIN_REJECT_RATE_POST: 'admin:reject:post-rate',
}
