const {getModel} = require('../../connections/database')

const Rate = getModel('Rate')

module.exports = async (rateId) => {
    const updatedData = {
        $set: {
            approved_at: Date.now(),
        },
        $unset: {
            rejected_at: 1,
        },
    }

    const newRate = await Rate.findByIdAndUpdate(rateId, updatedData, {new: true})

    if (!newRate) throw new Error(`rate: ${rateId} not found`)

    return newRate
}
