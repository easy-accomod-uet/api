const UserParser = require('./UserParser')
const {getModel} = require('../../connections/database')

const User = getModel('User')

const _update = (_id, data) => {
    return User
        .findByIdAndUpdate(
            _id,
            data,
            {new: true}
        )
        .lean()
}

module.exports = async (_id, user) => {
    const pUser = await UserParser.parseBase(user)

    const oldUser = await User.findById(_id).lean()

    if (!oldUser) throw new Error(`User:${_id} does not exists`)

    const newUser = await _update(_id, pUser)

    if (!newUser) throw new Error(`User: ${_id} not exists`)

    return newUser
}
