const {getModel} = require('../../connections/database')

const Rate = getModel('Rate')

module.exports = async (rateId) => {
    const updatedData = {
        $set: {
            rejected_at: Date.now(),
        },
        $unset: {
            approved_at: 1,
        }
    }

    const newRate = await Rate.findByIdAndUpdate(rateId, updatedData, {new: true})

    if (!newRate) throw new Error(`rate: ${rateId} not found`)

    return newRate
}
