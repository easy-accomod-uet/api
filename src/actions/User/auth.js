const {getModel} = require('../../connections/database')
const md5 = require('md5')
const _ = require('lodash')
const OAuth = require('../../middlewares/OAuth')

const User = getModel('User')

const findOneUser = async (query) => {
    return User.findOne(query).lean()
}

const makeQuery = (user) => {
    const {username} = user

    return {
        username: username,
        $or: [
            {
                is_deleted: {
                    $exists: false,
                },
            },
            {
                is_deleted: {
                    $exists: true,
                    $ne: true,
                }
            }
        ]
    }
}

const isThatYou = (password, passwordCrypted) => {

    return md5(password) === passwordCrypted
}

module.exports = async (user) => {
    const query = makeQuery(user)

    const userFound = await findOneUser(query)

    if (!userFound) throw new Error(`User not found.`)

    if (!isThatYou(user.password, userFound.password)) throw new Error(`Wrong password.`)

    const token = await OAuth.generateToken(_.pick(userFound, ['_id', 'username', 'first_name', 'last_name', 'role', 'createdAt']))

    return {
        data: {
            token: token,
            user: _.omit(userFound, ['password']),
        },
        meta: {
            body: user,
        }
    }
}
