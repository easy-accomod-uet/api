const {getModel} = require('../../connections/database')
const AddressParser = require('./AddressParser')

const Address = getModel('Address')

module.exports = async (_id, submit) => {
    const parsedSubmit = parseSubmit(submit)

    if (!parsedSubmit) throw new Error(`empty Submit update`)

    const oldSubmit = await Submit.findById(_id).lean()

    if (!oldSubmit) throw new Error(`submit:${_id} does not exists`)

    const vAddress = AddressParser.parseCreate(address)

    const newAddress = await createOneAddress(vAddress)

    return {
        data: {
            address: newAddress,
        },
    }
}
