const {getModel} = require('../../connections/database')

const Rate = getModel('Rate')

const find = (query) => {

    const find = Rate.find(query).populate('user')
        .sort({createdAt: -1})

    return find.lean()
}

module.exports = async (query) => {

    const data = await find(query)

    return data
}
