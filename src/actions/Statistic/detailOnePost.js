const parseDateQuery = require("../../helpers/parseDateQuery")
const Moment = require('../../helpers/Moment')
const PostParser = require('../Post/PostParser')
const {getModel} = require("../../connections/database")

const Post = getModel('Post')
const PostHistory = getModel('PostHistory')

const buildQuery = (postId, query = {}) => {
    const builtQuery = {
        post: postId,
    }

    const {['$gte']: from, ['$lte']: to} = parseDateQuery(query, true)

    query.by = getInterval({from, to})
    const {by} = query

    builtQuery.created_at = {
        '$gte': Moment(from).startOf(by),
        '$lte': Moment(to).endOf(by),
    }

    return builtQuery
}

const dateFormatMaps = {
    'month': 'MM',
    'day': 'DD',
    'hour': 'HH:mm',
    'minute': 'mm',
}

const getInterval = (range) => {
    const {from, to} = range

    const duration = to - from

    if (duration > 1000 * 60 * 60 * 24 * 30) return 'month'

    if (duration > 1000 * 60 * 60 * 24) return 'day'

    if (duration > 1000 * 60 * 60) return 'hour'

    return 'minute'
}

const getDateRange = (query, builtQuery) => {
    const {['$gte']: from, ['$lte']: to} = builtQuery.created_at

    const {by} = query

    const dateRange = Moment.range(Moment(from).startOf(by), Moment(to).endOf(by))

    const ranges = Array.from(dateRange.by(by))

    return ranges.map(e => e.toISOString())
}

const calculateRange = (query, dateRange, postHistories) => {
    const dateRangeMap = dateRange.reduce((prev, date) => {
        prev[date] = {
            history: null,
            dateText: date,
        }

        return prev
    }, {})

    postHistories.forEach(history => {

        const {created_at} = history

        const mCreated = Moment(created_at)

        const start = mCreated.startOf(query.by)

        if (dateRangeMap[start.toISOString()]) {
            dateRangeMap[start.toISOString()].history = history
        }
    })

    // calculate currently view, like
    const rangeHistories = Object.values(dateRangeMap)
    rangeHistories.forEach((value, index) => {

        const {history} = value

        if (index == 0) {

            if (!history) {
                value.history = postHistories[0]
            }

            value.views = 0
            value.likes = 0
        } else {
            lastValue = rangeHistories[index - 1]

            if (!history) {
                if (index !== rangeHistories.length - 1) {
                    value.history = lastValue.history
                    value.views = lastValue.views
                    value.likes = lastValue.likes

                    return
                }

                value.history = postHistories[postHistories.length - 1]

            }
            const {views, likes} = Object.assign({views: 0, likes: 0}, value.history)

            const {views: lastViews, likes: lastLikes} = Object.assign({views: 0, likes: 0}, lastValue.history)

            value.views = views - lastViews
            value.likes = likes - lastLikes
        }
    })

    return Object.keys(dateRangeMap).reduce((prev, e) => {
        prev[Moment(e).format(dateFormatMaps[query.by])] = dateRangeMap[e]

        return prev
    }, {})
}

module.exports = async (postId, query) => {

    const postFound = await Post.findById(postId).populate('author').populate('detail.address').populate('rates').lean()

    if (!postFound) throw new Error(`Post not found.`)

    const injectedPost = PostParser.injectBoolField(postFound)

    const builtQuery = buildQuery(postId, query)

    const postHistories = await PostHistory.find(builtQuery).sort({created_at: 1}).lean()

    console.log(postHistories.length)

    const dateRange = getDateRange(query, builtQuery)

    const resultRange = calculateRange(query, dateRange, postHistories)

    return {
        // post: injectedPost,
        aggregation: resultRange,
    }
}
