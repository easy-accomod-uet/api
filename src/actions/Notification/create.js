const {getModel} = require('../../connections/database')

const Notification = getModel('Notification')

module.exports = async (action, info, data) => {
    const {receiver, sender} = info
    const {key, description, meta} = data

    const newNoti = new Notification({
        receiver,
        sender,
        action,
        key,
        description,
        meta: meta || {},
    })

    await newNoti.save()

    return newNoti.toJSON()
}
