const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Post = getModel('Post')

const _findOne = (query, select) => {
    const find = Post.findOne(query).populate('detail.address').populate('author').populate('rates')

    if (select) find.select(select)

    return find.lean({virtuals: true})
}

module.exports = async (query, field) => {
    const parsedField = parseField(field)

    const found = await _findOne(query, parsedField)

    if (!found) throw new Error(`Post ${_id} of owner ${userId} not found`)

    return found

    // return {
    //     data: {
    //         post: found,
    //     },
    //     meta: {
    //         field: parsedField,
    //     }
    // }
}
