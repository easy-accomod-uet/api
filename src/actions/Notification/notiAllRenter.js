const {getModel} = require('../../connections/database')
const bulkCreate = require('./bulkCreate')

const User = getModel('User')

module.exports = async (action, info, data) => {
    const allRenter = await User.find({role: 'renter'}).select('_id').lean()

    info.receivers = allRenter.map(u => u._id)

    return bulkCreate(action, info, data)
}
