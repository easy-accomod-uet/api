const AddressParser = require('../Address/AddressParser')
const regexUtils = require('../../helpers/regexUtils')
const _ = require('lodash')
const Joi = require('joi')

const FeeSchema = Joi.object().keys({
    unit: Joi.string().trim().required(),
    amount: Joi.number().min(0).required(),
})

const RoomSchema = Joi.object().keys({
    shared: Joi.boolean().required(),
    quantity: Joi.number().default(1),
    meta: Joi.object().default({}),
})

const UtilitySchema = Joi.object().keys({
    name: Joi.string().trim().lowercase().required(),
    shared: Joi.boolean().required(),
    quantity: Joi.number().default(1),
    meta: Joi.object().default({}),
})

const AccommodationSchema = Joi.object().keys({
    address: Joi.alternatives().try(Joi.string(), AddressParser.AddressSchema).required(),
    nears: Joi.string().trim().default(''),
    type: Joi.string().required().allow("Phòng trọ", "Chung cư mini", "Nhà nguyên căn", "Chung cư nguyên căn"),
    quantity: Joi.number().min(1).required(),
    rent_fee: FeeSchema.required(),
    area: Joi.number().min(0).required(),
    shared: Joi.boolean().required(),
    bathroom: RoomSchema.required(),
    kitchen: RoomSchema.required(),
    electrict_fee: FeeSchema.required(),
    water_fee: FeeSchema.required(),
    utilities: Joi.array().items(UtilitySchema),
    images: Joi.array().items(Joi.string()).default([])
})

const ContactSchema = Joi.object().keys({
    full_name: Joi.string().trim().pattern(regexUtils.vietnamese).required(),
    phone: Joi.string().pattern(/^\+?\d+$/).min(8).max(20).required(),
})

const PostSchema = Joi.object().keys({
    author: Joi.string().required(),
    contact: ContactSchema.required(),
    title: Joi.string().trim().required(),
    description: Joi.string().trim().default(''),
    detail: AccommodationSchema.required(),
    expired_at: Joi.date().required(),
    check_by: Joi.string(),
    approved_at: Joi.date(),
    rejected_at: Joi.date(),
    rented_at: Joi.date(),
    views: Joi.number().integer().min(0).default(0),
    likes: Joi.number().integer().min(0).default(0),
    is_deleted: Joi.boolean(),
    updatedAt: Joi.date(),
    createdAt: Joi.date(),
})

const parseCreate = (data) => {
    if (!data) throw new Error('invalid data')

    return PostSchema.validateAsync(data)
}

const parseDTO = (data) => {
    if (!data) throw new Error('invalid data')

    return _.omit(data, ['__v', 'createdAt', 'updatedAt'])
}

exports.parseCreate = parseCreate

exports.parseDTO = parseDTO

exports.omitForbiddenField = (data) => {
    if (!data) throw new Error('invalid data')

    return _.omit(data, ['check_by', 'approved_at', 'rejected_at', 'rented_at', 'views', 'likes', 'is_deleted', 'updatedAt', 'createAt'])
}

const calculateRate = (rates) => {
    if (!Array.isArray(rates) || !rates.length) return null

    return Math.round(rates.reduce((prev, rate) => {
        const {rate: value} = rate

        return prev + value
    }, 0) / rates.length * 10) / 10
}

exports.injectBoolField = (data) => {
    if (!data) throw new Error('invalid data')

    return Object.assign({}, data, {is_approved: !!data.approved_at, is_rejected: !!data.rejected_at, is_rented: !!data.rented_at, rate: calculateRate(data.rates)})
}

exports.injectRateField = (data) => {
    if (!data) throw new Error('invalid data')

    return Object.assign({}, data, {is_approved: !!data.approved_at, is_rejected: !!data.rejected_at})
}
