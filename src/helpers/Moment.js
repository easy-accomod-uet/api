const momentRange = require('moment-range')
const moment = momentRange.extendMoment(require('moment'))

module.exports = moment
