const Checker = require('../../helpers/Checker')
const allowStatuses = ['pending', 'in-process', 'completed', 'failed', 'retry']
const allowLanguages = ['js', 'py', 'go', 'c', 'cpp']

module.exports = (submit) => {
    if (!submit) return null

    const parsed = {}

    // if (submit.hasOwnProperty('upload_file')) {
    //     parsed.upload_file = Checker.isString(submit.upload_file) ? submit.upload_file.trim() : null
    // }

    if (submit.hasOwnProperty('status')) {
        const pStatus = Checker.isString(submit.status) ? submit.status.trim().toLowerCase() : null

        if (!pStatus || !allowStatuses.includes(pStatus)) throw new Error('invalid status')

        parsed.status = pStatus
    }

    if (submit.hasOwnProperty('language')) {
        const pLanguage = Checker.isString(submit.language) ? submit.language.trim().toLowerCase() : null

        if (!pLanguage || !allowLanguages.includes(pLanguage)) throw new Error(`We do not support for language ${pLanguage}, allow ${allowLanguages}`)

        parsed.language = pLanguage
    }

    return Object.assign({}, submit, parsed)
}
