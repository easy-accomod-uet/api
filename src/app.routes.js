const express = require('express')
const BasicAuth = require('express-basic-auth')
const router = express.Router()
const {response} = require('./common')
// const Multer = require('./middlewares/Multer')
const OAuth = require('./middlewares/OAuth')

router.get('/', (_, res) => res.send(`Hi! I'm ${process.env.PROJECT_NAME}`))
router.get('/ping', (_, res) => res.send(`${process.env.PROJECT_NAME}:pong`))

const heartbeatCtl = require('./controllers/heartbeat')
router.get('/heartbeat/ping', heartbeatCtl.ping)
router.get('/heartbeat/ready', heartbeatCtl.ready)


const userCtrl = require('./controllers/user')
/**
 * No authenticated
 */
router.post('/users', userCtrl.create)
router.post('/register', userCtrl.create)
router.post('/auth', userCtrl.auth)
router.get('/users/:user_id', userCtrl.getById)

/**
 * Admin Route
 */
const adminCtrl = require('./controllers/admin')
router.post('/admin/create-user/private', BasicAuth({users: {'admin': 'codeatest_2020'}}), adminCtrl.createUser)
router.post('/admin/create-user', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.createUser)
router.post('/admin/users/search', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.searchUser)
router.get('/admin/users/:user_id', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.getUser)
router.put('/admin/users/:user_id', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.updateUser)
router.post('/admin/posts/search', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.searchPost)
router.post('/admin/posts/ranking', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.rankingPost)
router.get('/admin/posts/:post_id', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.getPost)
router.post('/admin/posts/:post_id/approve', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.approvePost)
router.post('/admin/posts/:post_id/reject', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.rejectPost)
router.post('/admin/posts/:post_id/restore', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.restorePost)
router.post('/admin/posts/:post_id/extend-expires', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.extendExpiresPost)
router.get('/admin/notifications', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.listNotification)
router.get('/admin/notifications/:notification_id', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.viewNotification)
router.post('/admin/rates/search', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.searchRates)
router.post('/admin/rates/:rate_id/approve', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.approveRatePost)
router.post('/admin/rates/:rate_id/reject', OAuth.isAuthenticated, OAuth.hasRole('admin'), adminCtrl.rejectRatePost)
// router.get('/users/:user_id/topic/:topic_id/quizzes', userCtrl.getQuizByTopic)
// router.get('/users/:user_id/submissions', userCtrl.getAllSubmission)
// router.get('/users/:user_id/quizzes/:quiz_id/submissions', userCtrl.getSubmissionOfQuiz)

/**
 * Me Route
 */
const meCtrl = require('./controllers/me')
router.get('/me', OAuth.isAuthenticated, meCtrl.getMe)
router.post('/me/change-password', OAuth.isAuthenticated, meCtrl.changePass)
router.post('/me/update-profile', OAuth.isAuthenticated, meCtrl.updateProfile)
router.get('/me/notifications', OAuth.isAuthenticated, meCtrl.listNotification)
router.get('/me/notifications/:notification_id', OAuth.isAuthenticated, meCtrl.viewNotification)
router.post('/me/favorites/search', OAuth.isAuthenticated, meCtrl.searchFavorite)
router.get('/me/favorites/:favorite_id', OAuth.isAuthenticated, meCtrl.getOneFavorite)
router.delete('/me/favorites/:favorite_id', OAuth.isAuthenticated, meCtrl.deleteFavorite)

/**
 * Owner Route
 */
const ownerCtrl = require('./controllers/owner')
router.post('/owner/posts', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.createPost)
router.post('/owner/posts/ranking', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.rankingPost)
router.post('/owner/posts/search', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.searchPost)
router.get('/owner/posts/:post_id', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.getPost)
router.post('/owner/statistics/posts/one/:post_id', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.statisticOnePost)
router.put('/owner/posts/:post_id', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.updatePost)
router.put('/owner/posts/:post_id/address', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.updateAddress)
router.get('/owner/notifications', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.listNotification)
router.get('/owner/notifications/:notification_id', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.viewNotification)
router.post('/owner/posts/:post_id/mark-rented', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.markPostRented)
router.post('/owner/posts/:post_id/request-extend-expires', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.requestExtendExpiresPost)
router.post('/owner/invoices/preview', OAuth.isAuthenticated, OAuth.hasRole('owner'), ownerCtrl.previewInvoice)

/**
 * Renter Route
 */
const renterCtrl = require('./controllers/renter')
router.post('/renter/posts/search', renterCtrl.searchPost)
router.get('/renter/posts/:post_id', renterCtrl.getPost)
router.get('/renter/posts/:post_id/like', renterCtrl.likePost)
router.get('/renter/posts/:post_id/rate', OAuth.isAuthenticated, renterCtrl.getRatePost)
router.post('/renter/posts/:post_id/rate', OAuth.isAuthenticated, renterCtrl.ratePost)
router.post('/renter/posts/:post_id/report', OAuth.isAuthenticated, renterCtrl.reportPost)
router.post('/renter/posts/:post_id/add-favorite', OAuth.isAuthenticated, renterCtrl.addToFavorite)

/**
 * 404 page.
 */
router.get('*', response.send404)

/**
 * Exports.
 */
module.exports = router
