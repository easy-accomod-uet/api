const {getModel} = require('../../connections/database')
const Joi = require('joi')

const Post = getModel('Post')

const ExpiredAtSchema = Joi.date().required()

const _findPost = (query, select) => {
    const find = Post.findOne(query)

    if (select) find.select(select)

    return find.lean()
}

const checkExpiredAt = (expiredAt) => ExpiredAtSchema.validateAsync(expiredAt)

module.exports = async (postId, post, authPayload) => {
    const postFound = await _findPost({_id: postId})

    if (!postFound) throw new Error(`Post ${postId} not found`)

    const {approved_at, rejected_at} = postFound

    if (!approved_at) throw new Error(`This post hasn't approved yet.`)

    if (rejected_at) throw new Error(`This post has been rejected.`)

    const {expired_at: expiredAt} = post

    const vExpiredAt = await checkExpiredAt(expiredAt)

    const updatedData = {
        $set: {
            expired_at: vExpiredAt,
        },
        $unset: {
            rejected_at: 1,
        },
    }

    const newPost = await Post.findByIdAndUpdate(postId, updatedData, {new: true})

    return newPost

    // setImmediate(() => {
    //     otificationAction.create('approve:post', {sender: userId, receiver: author}, {key: 'admin-approve-post', description: `Your Post: ${newPost.title} has been approved and will expires in ${newPost.expired_at}`, meta: {post: newPost, expired_at: vExpiredAt}})
    // })

    // return {
    //     data: true,
    //     meta: {
    //         body: post,
    //     }
    // }
}
