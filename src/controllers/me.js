const {response} = require('../common')
const UserAction = require('../actions/User')
const UserFavoriteAction = require('../actions/UserFavorite')
const NotificationAction = require('../actions/Notification')
const UserParser = require('../actions/User/UserParser')
const OAuth = require('../middlewares/OAuth')

exports.getMe = response.wrapper(async (req, res) => {
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId} = authPayload

    const userFound = await UserAction.getById(userId)

    return {
        data: {
            user: userFound
        },
    }
})

exports.changePass = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId} = authPayload

    const query = {
        _id: userId,
    }

    const newUser = await UserAction.changePass(query, _body)

    return {
        data: true,
        meta: {
            body: _body,
        }
    }
})

exports.updateProfile = response.wrapper(async (req, res) => {
    const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)

    const {_id: userId, role} = authPayload

    if (role === 'owner') throw new Error(`Owner cannot update self profile. Please contact to our supports.`)

    const vBody = UserParser.omitFieldForUpdateProfile(_body)

    const userFound = await UserAction.updateById(userId, vBody)

    return {
        data: {
            user: userFound,
        },
        meta: {
            body: _body,
        }
    }
})

exports.listNotification = response.wrapper(async (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    // const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    query.receiver = userId

    const {data, meta} = await NotificationAction.getList(query, {page, limit}, {field, sort})

    // setImmediate(async () => {

    //     NotificationAction.markAsViewed(data.list).catch(console.log)
    // })

    return {
        data: data,
        meta: meta
    }
})

exports.viewNotification = response.wrapper(async (req, res) => {
    const {notification_id: notificationId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    const query = {
        _id: notificationId,
        receiver: userId,
    }

    const oldNoti = await NotificationAction.getOne(query)

    let noti = oldNoti

    if (noti.is_new) {
        const markAsViewed = {
            is_new: false,
        }

        noti = await NotificationAction.updateById(oldNoti._id, markAsViewed)
    }

    return {
        data: {
            notification: noti,
        },
        meta: {
            params: req.params,
        }
    }
})

exports.searchFavorite = response.wrapper(async (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload
    query.user = userId

    const {data, meta} = await UserFavoriteAction.getList(query, {page, limit}, {field, sort})

    return {
        data: data,
        meta: meta
    }
})

exports.getOneFavorite = response.wrapper(async (req, res) => {
    const {favorite_id: favoriteId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    const favoriteFound = await UserFavoriteAction.getOneOfUserById(favoriteId, userId, field)

    return {
        data: favoriteFound,
        meta: {
            params: req.params,
        }
    }
})

exports.deleteFavorite = response.wrapper(async (req, res) => {
    const {favorite_id: favoriteId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)
    const {_id: userId} = authPayload

    await UserFavoriteAction.deleteOfUserById(favoriteId, userId)

    return {
        data: true,
        meta: {
            body: _body,
        }
    }
})
