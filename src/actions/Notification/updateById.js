const {getModel} = require('../../connections/database')

const Notification = getModel('Notification')

const updateById = (_id, data) => {
    return Notification
        .findByIdAndUpdate(
            _id,
            data,
            {new: true}
        )
        .lean()
}

module.exports = async (_id, body) => {
    const newNotification = await updateById(_id, body)

    if (!newNotification) throw new Error(`Notification ${_id} not exists`)

    return newNotification
}
