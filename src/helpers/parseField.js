const Checker = require('./Checker')

module.exports = (fields) => {
    if (!Checker.isString(fields)) return null

    return fields
        .split(/[,\s;.]/g)
        .map((ns) => (ns ? (ns + '').trim().toLowerCase() : ''))
        .filter(Boolean)
        .join(' ')
}

