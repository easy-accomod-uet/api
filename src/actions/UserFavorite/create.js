const {getModel} = require('../../connections/database')

const UserFavourite = getModel('UserFavourite')
const User = getModel('User')
const Post = getModel('Post')

const createOneUserFavourite = async (data) => {
    const newUserFavourite = new UserFavourite(data)

    const doc = await newUserFavourite.save()

    return doc.toJSON()
}

module.exports = async (data) => {
    const {post_id: postId, user_id: userId} = data

    if (!postId || !userId) throw new Error('invalid data.')

    const findUser = User.findById(userId).lean()
    const findPost = Post.findById(postId).lean()
    const findExists = UserFavourite.findOne({user: userId, post: postId}).lean()

    const [foundPost, foundUser, existsFound] = await Promise.all([findPost, findUser, findExists])

    if (!foundUser) throw new Error(`user: ${userId} not found`)
    if (!foundPost) throw new Error(`post: ${postId} not found`)
    if (existsFound) throw new Error(`this post exists in your favorite`)

    const newUserFavourite = await createOneUserFavourite({user: userId, post: postId})

    return newUserFavourite
}
