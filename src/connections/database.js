const {connector} = require('../common')
const database = require('@easyaccomod/schemas')

const MONGODB_URI = process.env.MONGODB_URI
console.log('MONGODB_URI:', MONGODB_URI)

const originConnection = connector.MongoDB(MONGODB_URI, {
    poolSize: 5,
    debug: true,
})

module.exports = database(originConnection)
