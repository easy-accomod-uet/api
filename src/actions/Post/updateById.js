const PostParser = require('./PostParser')
const {getModel} = require('../../connections/database')
const NotificationAction = require('../Notification')

const Post = getModel('Post')

const updatePostById = (_id, updatePost) => {
    return Post
        .findByIdAndUpdate(
            _id,
            Object.assign(updatePost, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (postId, post, authPayload, force = false) => {
    const {_id: userId} = authPayload

    post.author = userId

    const oldPost = await Post.findOne({_id: postId, author: userId}).populate('detail.address').lean()

    if (!oldPost) throw new Error(`post:${postId} of owner:${userId} does not exists`)

    if (!force && oldPost.approved_at) throw new Error(`cannot update approved post`)

    if (!force && oldPost.rejected_at) throw new Error(`cannot update rejected post`)

    const mergedPost = PostParser.parseDTO(Object.assign({}, oldPost, post))

    mergedPost.detail.address = oldPost.detail.address._id.toString()

    delete mergedPost._id

    const vPost = await PostParser.parseCreate(mergedPost)

    const newPost = await updatePostById(postId, vPost)

    if (!newPost) throw new Error(`Post ${_id} not exists`)

    return newPost

    // setImmediate(() => {
    //     NotificationAction.notiAllAdmin('update:post', {sender: userId}, {key: 'update-post', description: `Owner ${userId} update Post ${postId}`, meta: {post: newPost}})
    // })

    // return {
    //     data: {
    //         post: newPost,
    //     },
    // }
}
