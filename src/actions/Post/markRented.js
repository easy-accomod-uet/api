const {getModel} = require('../../connections/database')
const NotificationAction = require('../Notification')

const {actionEnum} = NotificationAction

const Post = getModel('Post')

const _findPost = (query, select) => {
    const find = Post.findOne(query)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (postId, body, authPayload) => {
    const {_id: userId} = authPayload

    const postFound = await _findPost({_id: postId, author: userId})

    if (!postFound) throw new Error(`Post ${postId} not found`)

    const {approved_at, rejected_at, rented_at} = postFound

    if (!approved_at) throw new Error(`This post hasn't approved yet.`)

    // if (rejected_at) throw new Error(`This post has been rejected.`)

    if (rented_at) throw new Error(`This post has rented already.`)

    const updatedData = {
        $set: {
            rented_at: Date.now(),
        },
    }

    const newPost = await Post.findByIdAndUpdate(postId, updatedData, {new: true})

    return newPost
}
