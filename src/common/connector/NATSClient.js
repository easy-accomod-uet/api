const NATS = require('nats')

module.exports = (args) => {
    const client = NATS.connect(args)

    client.on('connect', () => {
        console.log('NATS:connected')
    })

    client.on('disconnect', () => {
        console.log('NATS:disconnected')
    })

    client.on('close', () => {
        console.log('NATS:closed')
        process.exit(0)
    })

    client.on('error', (err) => {
        console.error('NATS:error')
        console.error(err)
    })

    client.on('reconnecting', function () {
        console.log('NATS:reconnecting')
    })

    client.on('reconnect', function (nc) {
        console.log('NATS:reconnect')
    })

    return client
}
