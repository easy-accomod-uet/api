const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Notification = getModel('Notification')

const _findOne = (query, select) => {
    const find = Notification.findOne(query)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (query, field) => {
    const parsedField = parseField(field)

    const found = await _findOne(query, parsedField)

    if (!found) throw new Error(`Notification not found`)

    return found
}
