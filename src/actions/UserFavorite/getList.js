const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')

const UserFavourite = getModel('UserFavourite')

const find = (query, {skip, limit}, {select, sort}) => {

    const find = UserFavourite.find(query).populate('user').populate({
        path: 'post',
        populate: {path: 'detail.address'}
    })
        .skip(skip)
        .limit(limit)
        .sort(sort || {createdAt: -1})

    if (select) find.select(select)

    return find.lean()
}

const count = query => {
    return UserFavourite.countDocuments(query)
}

module.exports = async (builtQuery, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [data, total] = await Promise.all([
        find(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        count(builtQuery),
    ])

    return {
        data: {
            list: data,
            total,
            page: parsedPaging.page,
            limit: parsedPaging.limit,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            page: parsedPaging.page,
            limit: parsedPaging.limit,
        }
    }
}
