const {getModel} = require('../../connections/database')
const AddressParser = require('./AddressParser')

const Address = getModel('Address')

const createOneAddress = async (address) => {
    const newAddress = new Address(address)

    const doc = await newAddress.save()

    return doc.toJSON()
}

module.exports = async (address) => {
    const vAddress = AddressParser.parseCreate(address)

    const newAddress = await createOneAddress(vAddress)

    return {
        data: {
            address: newAddress,
        },
    }
}
