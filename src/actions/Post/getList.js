const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')

const Post = getModel('Post')

const find = (query, {skip, limit}, {select, sort}) => {

    const find = Post.find(query).populate('author').populate('detail.address').populate('rates')
        .skip(skip)
        .limit(limit)
        .sort(sort || {createdAt: -1})

    if (select) find.select(select + ' approved_at rejected_at expired_at rented_at')

    return find.lean({virtuals: true})
}

const count = query => {
    return Post.countDocuments(query)
}

module.exports = async (builtQuery, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [data, total] = await Promise.all([
        find(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        count(builtQuery),
    ])

    return {
        data: {
            list: data,
            total,
            page: parsedPaging.page,
            limit: parsedPaging.limit,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            page: parsedPaging.page,
            limit: parsedPaging.limit,
        }
    }
}
