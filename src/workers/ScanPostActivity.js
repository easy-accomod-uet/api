const {getModel} = require('../connections/database')
const Logger = require('../services/Logger')

const Post = getModel('Post')
const PostHistory = getModel('PostHistory')
const Moment = require('../helpers/Moment')

let isRunning = false

const LIMIT = 100

let lastRunning = 0

const bulkCreateHistory = async (posts) => {

    const created = Moment(new Date()).startOf('minute').toISOString()

    const bulkInsertDoc = posts.map(p => {
        const {_id, views, likes} = p

        const doc = {
            post: _id.toString(),
            views,
            likes,
            created_at: (new Date(created)).toISOString(),
        }

        Logger.emit('scan-post', doc)

        return {
            insertOne: {
                document: doc
            }
        }
    })

    if ((Date.now() - lastRunning) > 1000 * 60 * 30) {
        await PostHistory.bulkWrite(bulkInsertDoc)
    }

    lastRunning = Date.now()

    return true
}

const run = async (lastId = null) => {
    const query = {
        approved_at: {
            $exists: true,
        },
        expired_at: {
            $gte: Date.now()
        },
        rejected_at: {
            $exists: false,
        },
    }

    if (lastId) {
        query._id = {
            $lt: lastId
        }
    }

    console.log(`run scan post activities, lastId: ${lastId}`)

    const posts = await Post.find(query).sort({_id: -1}).limit(LIMIT).lean()

    const len = posts.length

    console.log(`POST FOUND(${len})`)

    if (!len) return true

    await bulkCreateHistory(posts)

    if (len < LIMIT) return true

    const lastPost = posts[len - 1]

    return run(lastPost._id)
}

module.exports = async () => {
    if (isRunning) return true

    isRunning = true

    try {
        await run()

    } catch (e) {
        console.log(e)
    }

    isRunning = false

    return true
}
