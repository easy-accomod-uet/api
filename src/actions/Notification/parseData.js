const Checker = require('../../helpers/Checker')

module.exports = (testCase) => {
    if (!testCase) return null

    const parsed = {}

    if (testCase.hasOwnProperty('input')) {
        parsed.name = Checker.isString(testCase.name) ? testCase.name.trim() : null
    }

    if (testCase.hasOwnProperty('output')) {
        parsed.description = Checker.isString(testCase.description) ? testCase.description.trim() : null
    }

    return Object.assign({}, testCase, parsed)
}
