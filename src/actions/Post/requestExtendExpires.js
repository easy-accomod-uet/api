const {getModel} = require('../../connections/database')
const Joi = require('joi')
const NotificationAction = require('../Notification')

const {actionEnum} = NotificationAction

const Post = getModel('Post')

const ExpiredAtSchema = Joi.date().required()

const _findPost = (query, select) => {
    const find = Post.findOne(query)

    if (select) find.select(select)

    return find.lean()
}

const checkExpiredAt = (expiredAt) => ExpiredAtSchema.validateAsync(expiredAt)

const LIMIT_REQUEST = 5

module.exports = async (postId, post, authPayload) => {
    const {_id: userId} = authPayload

    const postFound = await _findPost({_id: postId, author: userId})

    if (!postFound) throw new Error(`Post ${postId} not found`)

    const {approved_at, rejected_at} = postFound

    if (!approved_at) throw new Error(`This post hasn't approved yet.`)

    if (rejected_at) throw new Error(`This post has been rejected.`)

    const {expired_at: expiredAt} = post

    const vExpiredAt = await checkExpiredAt(expiredAt)

    const numberOfRequestPending = await NotificationAction.count({sender: userId, action: actionEnum.OWNER_REQUEST_EXTEND_POST, is_new: true})

    if (numberOfRequestPending >= LIMIT_REQUEST) throw new Error(`You've reached the maximum pending request(${LIMIT_REQUEST}), please contact to our supports!`)

    await NotificationAction.notiAllAdmin(actionEnum.OWNER_REQUEST_EXTEND_POST, {sender: userId}, {key: 'post', description: `Owner: ${userId} request extend expires of Post: ${postFound._id} to ${vExpiredAt}`, meta: {post: postFound, expired_at: vExpiredAt}})

    return vExpiredAt
}
