const got = require('got')

const Requester = got.extend({
    prefixUrl: process.env.G12_URL || 'http://api.toedu.me',
    responseType: 'json',
    resolveBodyOnly: true,
})

exports.getUsers = async (users, token) => {

    const vUsers = users.filter(user => user !== '5fadeb3db2de4f60def024f9')

    const response = await Requester(`api/Intergrates/users`, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        json: vUsers
    })

    const {code, messsage, data} = response

    if (code !== 200) throw new Error(messsage)

    return data
}

// setImmediate(async () => {

//     console.log(await exports.getUsers(['08d88a53-7b20-4dac-86a0-2ddb7e68bb28'], "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJudmtoYWkyMDMiLCJ1c2VyX2lkIjoiMDhkODhhNTMtN2IyMC00ZGFjLTg2YTAtMmRkYjdlNjhiYjI4IiwiZW1haWwiOiJudmtoYWkyMDNAZ21haWwuY29tIiwibGlzdF9yb2xlcyI6InxTWVNfQURNSU4vR1JPVVAxMnwiLCJuYmYiOjE2MDc4NzY4NTQsImV4cCI6MTYwODQ4MTY1NCwiaWF0IjoxNjA3ODc2ODU0fQ.oaeBhw83SNJIhTQfUMA-07JKCgVe-jLo2HsF8KOWV8I"))

// })
